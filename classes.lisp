;;;; Views for classes
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

;;
;; Text reoresentation is the class name with package prefix,
;; unless the package is common-lisp
;;

(defmethod text-representation ((object class))
  (if-let ((symbol (class-name object)))
    (let ((*package* (find-package 'common-lisp)))
      (-> symbol prin1-to-string str:downcase))
    (call-next-method)))

;;
;; Source code view
;;

(defview 👀source (object class)
  (source-code-view object :in-file? t))

;;
;; Slot view
;;

(defview 👀slots (class class)
  (when (and (c2mop:class-finalized-p class)
             (c2mop:class-slots class))
    (-> class
        c2mop:class-slots
        thunk
        (list-view :title "Slots"
                   :priority 2))))

;;
;; Superclasses and subclasses
;;

(defview 👀superclasses (class class)
  (when (c2mop:class-direct-superclasses class)
    (-<> class
         superclass-tree
         cdr
         (cons nil <>)
         thunk
         tree-view
         (rename :title "Superclasses"
                 :priority 5))))

(defun superclass-tree (class)
  (cons class
        (mapcar #'superclass-tree (c2mop:class-direct-superclasses class))))

(defview 👀subclasses (class class)
  (when (c2mop:class-direct-subclasses class)
    (-<> class
         subclass-tree
         cdr
         (cons nil <>)
         thunk
         tree-view
         (rename :title "Subclasses"
                 :priority 6))))

(defun subclass-tree (class)
  (cons class
        (mapcar #'subclass-tree (c2mop:class-direct-subclasses class))))

;;
;; Slot definitions
;;

(defmethod text-representation ((object c2mop:slot-definition))
  (-> object c2mop:slot-definition-name str:downcase))

;;
;; Specializers
;;

(defview 👀specializing-methods (class class)
  (when-let (specializers (find-specializers class))
    (-> specializers
        (sort #'string< :key #'text-representation)
        (list-view :title "Methods"
                   :priority 8
                   :display #'text-representation))))
