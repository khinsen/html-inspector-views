;;;; Title bars for inspector panes
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views)

(defgeneric title-bar (object)
  (:documentation "Generate an HTML inline element for an object's
title bar. Defaults to the class name in italics followed by the
object's html-representation."))

(defgeneric title-bar-representation (object)
  (:documentation "Generate an HTML string representing the object in the
title bar of an inspector pane. Defaults to html-representation.")
  (:method ((object t))
    (html-representation object)))

(defmethod title-bar ((object t))
  (html
    (:button :id (inspect-id (class-of object))
             :class "inspector-title-bar-class"
             (:i (esc (string-downcase (class-name (class-of object))))))
    (:button :id (inspect-id object)
             :class "inspector-title-bar-object"
             :tabindex 0
             (title-bar-representation object))))

(defmethod title-bar :around ((object t))
  (let ((view (make-html-view (thunk (html-and-references (call-next-method)))
                                   :priority 0 :title "Title bar")))
    (setf (view-object view)
          object)
    (setf (view-method view)
          (first
           (remove-if #'method-qualifiers
                      (compute-applicable-methods
                       #'title-bar (list object)))))
    view))

(defgeneric title-bar-action-buttons (object)
  (:documentation "Generate HTML inline elements for action buttons in
an object's title bar. Defaults to no action buttons.")
  (:method ((object t))))

(defmethod title-bar-action-buttons :around ((object t))
  (let ((view (make-html-view (thunk (html-and-references (call-next-method)))
                                   :priority 0 :title "Action buttons")))
    (setf (view-object view)
          object)
    (setf (view-method view)
          (first
           (remove-if #'method-qualifiers
                      (compute-applicable-methods
                       #'title-bar (list object)))))
    view))
