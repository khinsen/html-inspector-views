;;;; Views for packages
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

(defmethod text-representation ((object package))
  (-> object package-name str:downcase))

;;
;; Functions, macros, classes, values, plists
;;

(defview 👀functions (package package)
  (when-let (function-symbols
             (symbols-satisfying #'(lambda (s) (and (fboundp (car s))
                                                    (not (macro-function (car s)))))
                                 package))
    (-> (symbol-value-view function-symbols #'symbol-function)
        (rename :title "Functions" :priority 2))))

(defview 👀macros (package package)
  (when-let (macro-symbols
             (symbols-satisfying #'(lambda (s) (and (fboundp (car s))
                                                    (macro-function (car s))))
                                 package))
    (when macro-symbols
      (-> (symbol-value-view macro-symbols #'symbol-function)
          (rename :title "Macros" :priority 3)))))

(defview 👀classes (package package)
  (when-let (class-symbols
             (symbols-satisfying #'(lambda (s) (find-class (car s) nil))
                                 package))
    (-> (symbol-value-view class-symbols
                           #'(lambda (s) (find-class s nil)))
        (rename :title "Classes" :priority 4))))

(defview 👀values (package package)
  (when-let (value-symbols
             (symbols-satisfying #'(lambda (s) (boundp (car s)))
                                 package))
    (-> (symbol-value-view value-symbols #'symbol-value)
        (rename :title "Values" :priority 5))))

(defview 👀plists (package package)
  (when-let (plist-symbols
             (symbols-satisfying #'(lambda (s) (symbol-plist (car s)))
                                 package))
    (-> (symbol-value-view plist-symbols #'symbol-plist)
        (rename :title "Plists" :priority 6))))

(defun symbols-satisfying (predicate package)
  (remove-if-not predicate
                 (loop for s being each present-symbol of package
                       collect (multiple-value-list 
                                (find-symbol (symbol-name s)
                                             package)))))

(defun symbol-value-view (symbols object-fn)
  (html-view
   (let* ((sorted (sort symbols #'string< :key #'first))
          (external (loop for (s kind) in sorted
                          when (equal kind :external)
                            collect s))
          (internal (loop for (s kind) in sorted
                          when (equal kind :internal)
                            collect s)))
     (when external
       (symbol-list-section external "External" object-fn))
     (when internal
       (symbol-list-section internal "Internal" object-fn)))))

(defun symbol-list-section (symbols title object-fn)
  (html
    (:details :open t
              (:summary (esc title))
              (:table :class "inspector-table"
                      (loop for s in symbols do
                        (html
                          (:tr (:td (object-ref s))
                               (:td (object-ref (funcall object-fn s))))))))))

;;
;; Unbound symbols
;;

(defview 👀unbound (package package)
  (when-let (unbound-symbols
             (symbols-satisfying #'(lambda (sk)
                                     (let ((s (car sk)))
                                       (not (or (boundp s)
                                                (fboundp s)
                                                (find-class s nil)))))
                                 package))
    (-> (symbol-value-view unbound-symbols
                           #'(lambda (_) (declare (ignore _)) ""))
        (rename :title "Unbound"
                :priority 6))))

;;
;; The package's use-list
;;

(defview 👀use-list (object package)
  (when-let (use-list (-> object package-use-list))
    (-> use-list
        (sort #'string< :key #'package-name)
        (list-view :title "Use list"
                   :priority 7))))

;;
;; The package's source code
;;

(defview 👀source (object package)
  (source-code-view object :in-file? t))

;;
;; The package's files
;;

(defview 👀files (pkg package)
  (multiple-value-bind (pathname offset)
      (source-code-location pkg)
    (declare (ignore offset))
    (when pathname
      (-<> pathname
           uiop:ensure-pathname
           pathname-directory
           (make-pathname :directory <>)
           👀items
           (rename :title "Files" :priority 9)))))
