;;;; View showing the elements of a list or vector
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

(defview 👀items (object cons)
  (cond
    ((alexandria:proper-list-p object)
     (enumerated-list-view (thunk object) :title "Items" :priority 5))
    ;; Show only car and cdr for anything else. Circular and improper
    ;; lists could be treated better.
    (t
     (key-value-table-view `(("car" . ,(car object))
                             ("cdr" . ,(cdr object)))
                           :title "Cons pair" :priority 5))))


(defmethod text-representation ((object cons))
  (if (or (consp (cdr object)) (null (cdr object)))
      (call-next-method)
      (str:concat "(" (text-representation (car object))
                  " . " (text-representation (cdr object)) ")")))
