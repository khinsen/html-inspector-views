#!/bin/sh

# https://beautifier.io / https://github.com/beautifier/js-beautify
wget https://cdnjs.cloudflare.com/ajax/libs/js-beautify/1.15.1/beautify.min.js \
     -O ./assets/html-inspector-views/js/beautify.min.js
wget https://cdnjs.cloudflare.com/ajax/libs/js-beautify/1.15.1/beautify-css.min.js \
     -O ./assets/html-inspector-views/js/beautify-css.min.js
wget https://cdnjs.cloudflare.com/ajax/libs/js-beautify/1.15.1/beautify-html.min.js \
     -O ./assets/html-inspector-views/js/beautify-html.min.js

# https://highlightjs.org / https://github.com/highlightjs/highlight.js
wget https://cdn.jsdelivr.net/gh/highlightjs/cdn-release@11.9.0/build/highlight.min.js \
     -O ./assets/html-inspector-views/js/highlight.min.js
wget https://cdn.jsdelivr.net/gh/highlightjs/cdn-release@11.9.0/build/languages/lisp.min.js \
     -O ./assets/html-inspector-views/js/highlight-lisp.min.js
wget https://cdn.jsdelivr.net/gh/highlightjs/cdn-release@11.9.0/build/styles/default.min.css  \
     -O ./assets/html-inspector-views/css/hljs-default.min.css

# https://viz-js.com/ / https://github.com/mdaines/viz-js
wget https://github.com/mdaines/viz-js/releases/download/release-viz-3.7.0/viz-standalone.js \
     -O ./assets/html-inspector-views/js/viz-standalone.js
