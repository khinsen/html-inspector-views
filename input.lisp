;;;; Input elements
;;
;;;; Copyright (c) 2024-2025 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views)

;;
;; An input-type HTML can optionally have a function argument. If provided,
;; an input event handler is set up that calls the function when the input
;; value changes, providing the new value as an argument to the function.
;;

(defun input-id (&optional fn)
  (html-id "input-" fn))

;;
;; Retrieve values from input elements
;; The actual function must be provided by the inspector,
;; by binding *get-input-fn*.
;;

(defvar *get-input-fn*)

(defun get-input (html-id)
  (when *get-input-fn*
    (funcall *get-input-fn* html-id)))

;;
;; Get/set view variables
;; The actual functions must be provided by the inspector,
;; by binding *get-view-var-fn* / *set-view-var-fn*.
;;

(defvar *get-view-var-fn*)

(defun get-view-var (kw)
  (when *get-view-var-fn*
    (funcall *get-view-var-fn* kw)))

(defvar *set-view-var-fn*)

(defun set-view-var (kw value)
  (when *set-view-var-fn*
    (funcall *set-view-var-fn* kw value)))

;;
;; Refreshable parametrized HTML snippets
;;

(defclass parametrized-subview ()
  ((view-vars :reader view-vars :initarg :view-vars)
   (thunk :reader view-thunk :initarg :view-thunk)))

(defmacro parametrized-subview (view-vars &body body)
  (assert (consp view-vars))
  (dolist (v view-vars) (assert (keywordp v)))
  (let ((html-id-symbol (gensym))
        (snippet-symbol (gensym)))
    `(let* ((,snippet-symbol (make-instance 'parametrized-subview
                                            :view-vars (quote ,view-vars)
                                            :view-thunk (thunk ,@body)))
            (,html-id-symbol (symbol-name (gensym "pview-"))))
       (html
         (:div :id ,html-id-symbol))
       (push (cons ,html-id-symbol ,snippet-symbol)
             (accumulator-references *view-accumulator*)))))

;;
;; Input elements
;;

(defun select (view-var label choices)
  (let ((html-id (input-id view-var)))
    (html
      (:div
       (:label :for html-id (esc label))
       (:select :id html-id
         (loop for c across (coerce choices 'vector)
               do (if (consp c)
                      (html
                        (:option :value (cdr c) (esc (car c))))
                      (html
                        (:option (esc c))))))))))

(defun checkboxes (view-var label choices &key (checked nil))
  (html
    (:div
     (:fieldset
      (:legend (esc label))
      (loop for c across (coerce choices 'vector)
            for index from 0
            do (let ((html-id (input-id (cons view-var index))))
                 (if (consp c)
                     (html
                       (:div
                        (:input :type "checkbox"
                                :id html-id
                                :value (cdr c)
                                :checked (member (car c) checked :test #'string= ))
                        (:label :for html-id (esc (car c)))))
                     (html
                       (:div
                        (:input :type "checkbox"
                                :id html-id
                                :checked (member (car c) checked :test #'string= ))
                        (:label :for html-id (esc c)))))))))))
