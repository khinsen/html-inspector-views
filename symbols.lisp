;;;; Views for symbols
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

(defmethod text-representation ((symbol symbol))
  (if-let (package (-> symbol symbol-package))
    (if (string= (-> package package-name)
                 "KEYWORD")
        (str:concat ":" (symbol-name symbol))
        (symbol-name symbol))
    (str:concat "#:" (symbol-name symbol))))

(defview 👀components (object symbol)
  (-> (key-value-table-view (thunk (symbol-components object))
                            :title "Components"
                            :priority 1)))

(defun symbol-components (s)
  (list (cons "Name" (symbol-name s))
        (cons "Package" (symbol-package s))
        (cons "Value" (handler-case (symbol-value s) (unbound-variable (c) c)))
        (cons "Function" (handler-case (symbol-function s) (undefined-function (c) c)))
        (cons "Plist" (symbol-plist s))))

