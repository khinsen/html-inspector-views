;;;; Views for numbers
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

(defview 👀representations (n integer)
  (key-value-table-view
   (thunk `(("Decimal" . ,(princ-to-string n))
            ("Hex" . ,(format nil "#x~x" n))
            ("Octal" . ,(format nil "#o~o" n))
            ("Binary" . ,(format nil "#b~b" n))
            ("Universal time" . ,(format nil "~a" (local-time:universal-to-timestamp n)))
            ("Unix time" . ,(format nil "~a" (local-time:unix-to-timestamp n)))))
   :title "Representations" :priority 1))

(defview 👀representations (q rational)
  (key-value-table-view
   (thunk `(("Fraction" . ,(princ-to-string q))
            ("Float" . ,(format nil "~10$" q))))
   :title "Representations" :priority 1))


