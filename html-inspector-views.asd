;;;; System definitions
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(asdf:defsystem #:html-inspector-views
  :description "Infrastructure for defining views for moldable HTML-based inspectors"
  :author "Konrad Hinsen <konrad.hinsen@fastmail.net>"
  :license  "BSD"
  :version "0.0.1"
  :homepage "https://codeberg.org/html-inspector-views/"
  :serial t
  :depends-on (#:cl-who
               #:cl-base32
               #:flexi-streams
               #:str)
  :components ((:file "package")
               (:file "thunks")
               (:file "html")
               (:file "title-bar")
               (:file "views")
               (:file "view-support")
               (:file "input")
               (:file "gui-class")))

(asdf:defsystem #:html-inspector-views/templates
  :description "HTML templates for inspector views"
  :author "Konrad Hinsen <konrad.hinsen@fastmail.net>"
  :license  "BSD"
  :version "0.0.1"
  :serial t
  :depends-on (#:html-inspector-views
               #:eco)
  :components ((:file "package-templates")
               (:file "templates")))

(asdf:defsystem #:html-inspector-views/standard
  :description "HTML inspector views for standard Common Lisp objects"
  :author "Konrad Hinsen <konrad.hinsen@fastmail.net>"
  :license  "BSD"
  :version "0.0.1"
  :serial t
  :depends-on (#:html-inspector-views
               #:alexandria
               #:arrow-macros
               #:cl-base64
               #:cl-who
               #:closer-mop
               #:eclector-concrete-syntax-tree
               #:fset
               #:local-time
               #:s-graphviz
               #:str
               #:swank
               #:trivial-clipboard
               #:asdf #:uiop)
  :components ((:file "package-standard")
               (:file "svg")
               (:file "basic")
               (:file "pathnames")
               (:file "numbers")
               (:file "characters")
               (:file "strings")
               (:file "symbols")
               (:file "lists")
               (:file "arrays")
               (:file "hash-tables")
               (:file "swank")
               (:file "functions")
               (:file "classes")
               (:file "packages")
               (:file "image")
               (:file "systems")
               (:file "conditions")
               (:file "views-on-views")
               #+quicklisp (:file "quicklisp")
               (:file "s-graphviz")
               (:file "fset")
               (:file "hyperspec")
               (:file "lisp-code")
               (:file "json")))
