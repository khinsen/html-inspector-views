;;;; Commonly used configurable views
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views)

;;
;; Generic and HTML view classes
;;

(defclass view ()
  ((object :accessor view-object)
   (method :accessor view-method)
   (title :accessor view-title :initarg :title)
   (priority :accessor view-priority :initarg :priority)
   (assets :accessor view-assets :initarg :assets :initform nil)))

(defmethod text-representation ((object view))
  (view-title object))

(defclass html-view (view)
  ((html :accessor view-html :initarg :html)
   (references :accessor view-references :initarg :references)
   (create-html :reader view-create-html :initarg :create-html)))

(defun make-html-view (fn &key (title "View") (priority 99))
  (make-instance 'html-view
                 :html nil
                 :references nil
                 :assets nil
                 :create-html fn
                 :title title
                 :priority priority))

(defun evaluate-lazy-view (view)
  (multiple-value-bind (html references assets)
      (eval-thunk (view-create-html view))
    (setf (view-html view) html)
    (setf (view-references view) references)
    (setf (view-assets view) assets)))

(defmethod view-html :before ((view html-view))
  (unless (slot-value view 'html)
    (evaluate-lazy-view view)))

(defmethod view-references :before ((view html-view))
  (unless (slot-value view 'html)
    (evaluate-lazy-view view)))

(defmethod view-assets :before ((view html-view))
  (unless (slot-value view 'html)
    (evaluate-lazy-view view)))

;;
;; Change the name and priority of a view
;;

(defun rename (view &key title priority)
  (when view
    (when title (setf (view-title view) title))
    (when priority (setf (view-priority view) priority))
    view))

(defun priority (view priority)
  (when view
    (setf (view-priority view) priority)
    view))

;;
;; List of registered view functions
;;

(defvar *view-functions* '())

;;
;; Retrieve all views for an object
;;

(defun all-views (object)
  (let* ((views (mapcar #'(lambda (f) (funcall f object)) *view-functions*))
         (non-nil-views (remove-if-not #'(lambda (x) x) views)))
    (sort non-nil-views #'< :key #'view-priority)))

;; Macro for defining (generic) view functions
;;
;; It defines a default method for class T that returns nil,
;; ensuring that a view function never fails due to a missing
;; method implementation.
;;

(defmacro defview (symbol arg &rest body)
  `(progn (unless (member (quote ,symbol) *view-functions*)
            (defgeneric ,symbol (object)
              (:method ((object t)) nil)
              (:method :around ((object t))
                (let ((view (call-next-method)))
                  (when view
                    (setf (view-object view)
                          object)
                    (setf (view-method view)
                          (first (compute-applicable-methods
                                  (function ,symbol) (list object))))
                    view))))
            (push (quote ,symbol) *view-functions*))
          (defmethod ,symbol (,arg)
            ,@body)))

;;
;; Views for basic data types from html-inspector-views/standard
;; Defined here for class T so that they can be called from
;; custom views.
;;

;; Items of a list, vector, or hash table
(defview 👀items (object t)
  nil)

;; Description of a hash table
(defview 👀description (object t)
  nil)

;; Representations of a number
(defview 👀representations (object t)
  nil)

;; Content of a pathname
(defview 👀content (object t)
  nil)

;; Source (code) of a function, method, package, ...
(defview 👀source (object t)
  nil)

;; Generic function for configuring the operations view
(defgeneric open-package-operations? (object package-name))

;; Printed representation of an object
(defview 👀print-string (object t)
  nil)

