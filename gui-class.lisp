;;;; GUI base class
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views)

;;
;; GUI classes are classes created mainly to serve as a support
;; for views. By adding this base class, standard object views
;; such as print-string and slots are suppressed, reducing visual
;; clutter. Moreover, the title bar of the inspector pane shows
;; nothing but the pane title defined for the class.
;;

(defclass gui-class ()
  ((pane-title :accessor pane-title :initarg :pane-title)))

(defmethod title-bar ((object gui-class))
  (html
    (:button :id (inspect-id object)
             :class "inspector-title-bar-object"
             (esc (pane-title object)))))
