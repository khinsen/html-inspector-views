# html-inspector-views

Infrastructure for defining inspector views in terms of HTML snippets. Requires an inspector implementation that can use these views, such as [clog-moldable-inspector](https://codeberg.org/khinsen/clog-moldable-inspector).

This repository contains three systems:
 - `html-inspector-views` is a very small system with a single dependency ([cl-who](https://edicl.github.io/cl-who/)). It contains only
   what is required to define your own views.
 - `html-inspector-views/standard` contains views for objects that are part of the
   Common Lisp standard: numbers, symbols, lists, functions, etc., plus objects from [Quicklisp](https://www.quicklisp.org/beta/) if you use it.
 - `html-inspector-views/fset` contains views for objects of [FSet](https://gitlab.common-lisp.net/fset/fset/-/wikis/home), a library implementing functional collections (aka immutable or persistent data structures).
 
Rule of thumb: Use `html-inspector-views` in systems that define views, use `html-inspector-views/standard` in inspector implementations. Load `html-inspector-views/fset` if you use FSet.

Implementing a view requires two parts: a `defview` form that defines the view and documents it, and a `defmethod` form that provides an implementation for some class. See [basic.lisp](basic.lisp) for two examples.

A view contains four pieces of information:
 - a title, which is shown in the view's tab in an inspector pane
 - a priority, which is a number that defines the view's place in the pane (the views are
   simply sorted by priority)
 - an HTML snippet defining the view
 - an [Alist](https://lispcookbook.github.io/cl-cookbook/data-structures.html#alist) mapping HTML element ids to Lisp objects
 
Any HTML element in the view whose id has an entry in the Alist is associated with a mouse click action that inspects the object referenced in the Alist.
 

## License

[BSD](./LICENSE)

The files
 - `assets/html-inspector-views/js/beautify.min.js`
 - `assets/html-inspector-views/js/beautify-css.min.js`
 - `assets/html-inspector-views/js/beautify-html.min.js`
are from the [js-beautify](https://github.com/beautifier/js-beautify) project and covered by an [MIT license](https://github.com/beautifier/js-beautify/blob/main/LICENSE).

The files
 - `assets/html-inspector-views/js/highlight.min.js`
 - `assets/html-inspector-views/js/highlight-lisp.min.js`
 - `assets/html-inspector-views/css/hljs-default.min.css`
are from the [highlight.js](https://github.com/highlightjs/highlight.js) project and covered by a [BSD 3-clause license](https://github.com/highlightjs/highlight.js/blob/main/LICENSE).

The file
 - `assets/html-inspector-views/js/viz-standalone.js`
is from the [Viz.js](https://github.com/mdaines/viz-js) project and covered by an [MIT license](https://github.com/mdaines/viz-js/blob/v3/LICENSE).

See [download-assets.sh](./download-assets.sh) for how these files were obtained.
