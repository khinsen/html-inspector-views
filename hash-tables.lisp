;;;; Views for hash tables
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

;;
;; Text representation
;;

(defmethod text-representation ((ht hash-table))
  (format nil "hash-table :count ~a :test ~a"
          (hash-table-count ht)
          (hash-table-test ht)))

;;
;; Items (key-value pairs)
;;

(defview 👀items (ht hash-table)
  (unless (zerop (hash-table-count ht))
    (html-view :title "Items" :priority 1
     (html
      (:table :class "inspector-table"
              (loop for key being the hash-keys in ht
                      using (hash-value value)
                    do (htm
                        (:tr (:td (object-ref key))
                             (:td (object-ref value))))))))))

;;
;; Description (count, equality test)
;;

(defview 👀description (ht hash-table)
  (-> (key-value-table-view
       (thunk `(("Count" . ,(hash-table-count ht))
                ("Size" . ,(hash-table-size ht))
                ("Test" . ,(hash-table-test ht))
                ("Rehash size" . ,(hash-table-rehash-size ht))
                ("Rehash threshold" . ,(hash-table-rehash-threshold ht))))
       :title "Description" :priority 2)))
