;;;; Generic views applicable to many classes
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

;;
;; The slots of any object
;;

(defview 👀slots (object t)
  (when-let (values (slot-values object))
    (-> (key-value-table-view (thunk values)
                              :title "Slots"
                              :priority 10))))

(defun slot-values (object)
  (when-let ((names (slot-names object)))
    (mapcar #'(lambda (name)
                (cons (-> name symbol-name str:downcase)
                      (handler-case (slot-value object name) (unbound-slot (c) c))))
            names)))

(defun slot-names (object)
  (->> object
       class-of
       c2mop:class-slots
       (mapcar #'c2mop:slot-definition-name)))

;; Suppress the view for GUI classes
(defview 👀slots (object gui-class)
  nil)

;;
;; The printed representation of an object
;;

(defview 👀print-string (object t)
  (html-view :title "Print" :priority 90
    (html
     (:div :class "inspector-text"
           (esc (princ-to-string object))))))

;; Suppress the view for GUI classes
(defview 👀print-string (object gui-class)
  nil)

;;
;; Methods directly runnable on an object
;;

(defview 👀operations (object t)
  (html-view :title "Operations" :priority 95
    (let ((m-groups (-<> object
                         find-applicable-methods
                         (remove-if-not #'callable-with-one-arg? <>)
                         (fset:convert 'fset:seq <>)
                         (fset:sort-and-group #'string<
                                              :key #'method-package-name))))
      (html
        (fset:do-seq (methods m-groups)
          (let* ((package (method-package (fset:first methods)))
                 (package-name (package-name package)))
            (flet ((m-in-p (method)
                     (let ((*package* package))
                       (method-fn-name method))))
              (html
                (:details
                 :open (open-package-operations? object package-name)
                 (:summary (esc (str:downcase package-name)))
                 (:table :class "inspector-table"
                         (fset:do-seq (method (fset:stable-sort methods
                                                                #'string<
                                                                :key #'m-in-p))
                           (html
                             (:tr (:td (eval-button
                                        *icon-eval*
                                        (thunk
                                         (funcall
                                          (c2mop:method-generic-function method)
                                          object))))
                                  (:td (object-ref method
                                                   :display #'m-in-p)))))))))))))))

(defun callable-with-one-arg? (method)
  (= 1 (length (c2mop:method-specializers method))))

(defun method-package (method)
  (let ((fn-name (-> method c2mop:method-generic-function function-name)))
    (cond
      ((symbolp fn-name) (symbol-package fn-name))
      ((and (consp fn-name)
            (equal 'cl:setf (car fn-name))
            (symbolp (cadr fn-name)))
       (symbol-package (cadr fn-name)))
      (t nil))))

(defun method-package-name (method)
  (package-name (method-package method)))

(defmethod open-package-operations? ((object t) package-name)
  (let ((class-package-name (-> object
                                class-of
                                class-name
                                symbol-package
                                package-name)))
    (string= package-name class-package-name)))

;; Suppress the view for GUI classes
(defview 👀operations (object gui-class)
  nil)
