;;;; Views for arrays
;;
;;;; Copyright (c) 2025 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

(defview 👀items (object vector)
  (unless (zerop (length object))
    (enumerated-list-view (thunk object) :title "Items" :priority 5)))

(defview 👀description (object array)
  (key-value-table-view
   (thunk `(("Element type" . ,(array-element-type object))
            ("Rank" . ,(array-rank object))
            ("Dimensions" . ,(array-dimensions object))
            ("Total size" . ,(array-total-size object))
            ("Adjustable?" . ,(adjustable-array-p object))
            ,@(when (array-has-fill-pointer-p object)
                `(("Fill pointer" . ,(fill-pointer object))))
            ,@(multiple-value-bind (displaced-to offset)
                  (array-displacement object)
                (when displaced-to
                  `(("Displaced to" . ,displaced-to)
                    ("  index offset" . ,offset))))))
   :title "Properties" :priority 6))
