;;;; Views for HyperSpec pages
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

(defclass hyperspec-page (gui-class)
  ((symbol :initarg :symbol)
   (url :initarg :url)))

(defmethod text-representation ((page hyperspec-page))
  (with-slots (symbol) page
    (str:downcase (symbol-name symbol))))

(defmethod pane-title ((page hyperspec-page))
  (with-slots (symbol) page
    (format nil "HyperSpec page for ~A"
            (text-representation page))))

(defview 👀content (page hyperspec-page)
  (html-view :title "Content" :priority 1
    (html
      (:iframe :src (slot-value page 'url)
               :title (text-representation page)
               :style "border:none;width:100%;height:100%" ))))

(defun make-hyperspec-page (symbol)
  (make-instance 'hyperspec-page
                 :symbol symbol
                 :url (str:concat "http://l1sp.org/cl/"
                                  (str:downcase (symbol-name symbol)))))
