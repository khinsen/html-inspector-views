;;;; Views for Quicklisp
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

;;
;; GUI class for inspecting Quicklisp
;;

(defclass quicklisp (gui-class) ())

(defvar *quicklisp* (make-instance 'quicklisp :pane-title "Quicklisp"))

;; Available releases, installed ones show a checkmark
(defview 👀releases (object (eql *quicklisp*))
  (-> t
      ql-dist:provided-releases
      thunk
      (multi-column-list-view
       :title "Releases"
       :priority 1
       :columns '(" " "Name" "Distribution")
       :display (list #'(lambda (release)
                          (if (ql-dist:installedp release) "✓" ""))
                      #'identity
                      (alexandria:compose #'ql-dist:name #'ql-dist:dist)))))

;; Available systems, installed ones show a checkmark
(defview 👀systems (object (eql *quicklisp*))
  (-> t
      ql-dist:provided-systems
      thunk
      (multi-column-list-view
       :title "Systems"
       :priority 2
       :columns '(" " "Name" "Distribution")
       :display (list #'(lambda (system)
                          (if (ql-dist:installedp system) "✓" ""))
                      #'identity
                      (alexandria:compose #'ql-dist:name #'ql-dist:dist)))))

;; Available distributions with their preference values
(defview 👀distributions (object (eql *quicklisp*))
  (-> (ql-dist:all-dists)
      thunk
      (multi-column-list-view
       :title "Distributions"
       :priority 5
       :columns '("Name" "Preference")
       :display (list #'identity
                      #'ql-dist:preference))))

;; Available updates
(defview 👀updates (object (eql *quicklisp*))
  (when-let (updates
             (-<> (ql-dist:all-dists)
                  (mapcar #'available-update <>)
                  (remove nil <>)))
    (multi-column-list-view
     updates
     :title "Updates"
     :priority 7
     :columns '("Distribution" "From" "To")
     :display (list #'ql-dist:name
                    #'(lambda (d)
                        (-> d
                            current-version
                            ql-dist:version))
                    #'ql-dist:version))))

;; Local projects
(defview 👀local-projects (object (eql *quicklisp*))
  (when-let (local (ql:list-local-projects))
    (-> local
        (sort #'string< :key #'pathname-name)
        (list-view :title "Local projects"
                   :priority 10
                   :display #'pathname-name))))

;;
;; Distributions
;;

;; Network errors are converted to "no update available"
(defun available-update (dist)
  (handler-case (ql-dist:available-update dist)
    (error (c) (declare (ignore c)) nil)))

;; Current version of a distribution
(defun current-version (dist)
  (-> dist ql-dist:name ql-dist:find-dist))

;; Test if a distribution is an update to an installed one
(defun is-update? (dist)
  (string-lessp (-> dist current-version ql-dist:version)
                (-> dist ql-dist:version)))

;; Text representation is name + version
(defmethod text-representation ((dist ql-dist:dist))
  (format nil "~a ~a"
          (ql-dist:name dist)
          (ql-dist:version dist)))

;; HTML represation adds "disabled" in red for disabled distributions,
(defmethod html-representation ((dist ql-dist:dist) &optional id)
  (declare (ignore id))
  (let ((standard (call-next-method)))
    (if (ql-dist:enabledp dist)
        standard
        (html
          (str standard)
          (:span :class "inspector-error"
                 (if (is-update? dist)
                     (esc " (update)")
                     (esc " (disabled)")))))))

;; Add an enable/disable button to the title bar
(defmethod title-bar-action-buttons ((dist ql-dist:dist))
  (if (ql-dist:enabledp dist)
      (action-button "Disable"
                     (thunk (ql-dist:disable dist)
                            t))
      (unless (is-update? dist)
        (action-button "Enable"
                       (thunk (ql-dist:enable dist)
                              t)))))

;; Releases, in alphabetical order
(defview 👀releases (dist ql-dist:dist)
  (-> dist
      ql-dist:provided-releases
      copy-list
      (sort #'string< :key #'ql-dist:prefix)
      thunk
      (multi-column-list-view
       :title "Releases"
       :priority 1
       :columns '(" " "Name")
       :display (list #'(lambda (release)
                          (if (ql-dist:installedp release) "✓" ""))
                      #'identity))))

;; Systems, in alphabetical order
(defview 👀systems (dist ql-dist:dist)
  (-> dist
      ql-dist:provided-systems
      copy-list
      (sort #'string< :key #'ql-dist:name)
      thunk
      (multi-column-list-view
       :title "Systems"
       :priority 2
       :columns '(" " "Name")
       :display (list #'(lambda (system)
                          (if (ql-dist:installedp system) "✓" ""))
                      #'identity))))

;; The versions that are available locally
(defview 👀versions (dist ql-dist:dist)
  (when-let ((versions (available-versions dist)))
    (html-view :title "Versions" :priority 3
     (html
        (:small :class "inspector-index"
                (fmt "~a items" (length versions)))
        (:table :class "inspector-table"
                (loop for (date . url) in versions do
                      (html
                        (:tr (:td (:a :href url :target "_blank" (esc date)))))))))))

;; Network errors are converted to "no versions available"
(defun available-versions (dist)
  (handler-case (ql-dist:available-versions dist)
    (error (c) (declare (ignore c)) nil)))

;; Local files defining a distribution
(defview 👀files (dist ql-dist:dist)
  (-> dist
      ql-dist:base-directory
      👀items
      (rename :title "Files" :priority 5)))

;; Update view if an update is available
(defview 👀update (dist ql-dist:dist)
  (when (is-update? dist)
    (let ((current (current-version dist)))
      (multiple-value-bind (new updated removed)
          (ql-dist:update-release-differences current dist)
        (html-view :title "Update" :priority 0
          (html
            (:span
             (action-button (format nil "Install update to ~a" (ql-dist:version dist))
                            (thunk (ql-dist:update-in-place current dist)
                                   t)))
            (when new
              (cl-who:htm
               (:h4 "New")
               (-> new
                   (sort #'string< :key #'ql-dist:prefix)
                   (html-table :display (list #'ql-dist:prefix)))))
            (when updated
              (cl-who:htm
               (:h4 "Updated")
               (:table :class "inspector-table"
                       (:tr (:th "Current") (:th "New"))
                       (let ((sorted (sort updated
                                           #'string< :key
                                           #'(lambda (u) (ql-dist:prefix (first u))))))
                         (loop for item in sorted do
                           (htm
                            (:tr (:td (object-ref (first item)
                                                  :display #'ql-dist:prefix))
                                 (:td (object-ref (second item)
                                                  :display #'ql-dist:prefix)))))))))
            (when removed
              (cl-who:htm
               (:h4 "Removed")
               (-> removed
                   (sort #'string< :key #'ql-dist:prefix)
                   (html-table :display (list #'ql-dist:prefix)))))))))))

;;
;; Releases
;;

;; The text representation is the name of the release
(defmethod text-representation ((release ql-dist:release))
  (ql-dist:name release))

;; Add the distribution to the titlebar
(defmethod title-bar-representation ((release ql-dist:release))
  (html (:span
         (esc (ql-dist:prefix release))
         (:small (esc " from ")
                 (esc (ql-dist:name (ql-dist:dist release)))))))

;; Add an (un)install butto to the titlebar
(defmethod title-bar-action-buttons ((release ql-dist:release))
  (if (ql-dist:installedp release)
      (action-button "Uninstall"
                     (thunk (ql-dist:uninstall release)
                            t))
      (action-button "Install"
                     (thunk (ql-dist:install release)
                            t))))

;; The systems defined in the release
(defview 👀systems (release ql-dist:release)
  (-> release
      ql-dist:provided-systems
      thunk
      (list-view :title "Systems" :priority 1)))

;; System (.asd) files. For installed releases, make the list point
;; to the actual pathnames for easy browsing. And if there is only one
;; system file, show it immediately.
(defview 👀system-files (release ql-dist:release)
  (when-let (sysfiles (ql-dist:system-files release))
    (if (ql-dist:installedp release)
        (if (= 1 (length sysfiles))
            (system-file-view (first sysfiles) (ql-dist:base-directory release))
            (system-file-list-view sysfiles (ql-dist:base-directory release)))
        (system-filename-list-view sysfiles))))

(defun system-file-view (sysfile basedir)
  (-> sysfile
      (merge-pathnames basedir)
      👀content
      (rename :title "System file" :priority 3)))

(defun system-file-list-view (sysfiles basedir)
  (-<> sysfiles
       (mapcar #'(lambda (f) (merge-pathnames f basedir)) <>)
       thunk
       (list-view :title "System files" :priority 3
                  :display #'file-namestring)))

(defun system-filename-list-view (sysfiles)
  (-<> sysfiles
       (mapcar #'parse-namestring <>)
       thunk
       (list-view :title "System files" :priority 3)))

;; For installed releases, show the top-level directory
(defview 👀files (release ql-dist:release)
  (when (ql-dist:installedp release)
    (-> release
        ql-dist:base-directory
        👀items
        (rename :title "Files" :priority 5))))

;; Other releases with the same name
(defview 👀homonyms (release ql-dist:release)
  (let ((releases (ql-dist::find-releases-named (ql-dist:name release))))
    (if (> (length releases) 1)
        (multi-column-list-view (thunk releases)
         :title "Homonyms"
         :priority 5
         :columns '("Preference" "Distribution")
         :display (list #'ql-dist:preference
                        (alexandria:compose #'ql-dist:name #'ql-dist:dist)))
        nil)))

;;
;; Systems
;;

;; The text representation is the name of the system
(defmethod text-representation ((system ql-dist:system))
  (ql-dist:name system))

;; Add the distribution/release to the titlebar
(defmethod title-bar-representation ((system ql-dist:system))
  (call-next-method)
  (html (:small (fmt " from ~a:~a"
                     (ql-dist:name (ql-dist:dist system))
                     (ql-dist:name (ql-dist:release system))))))

;; Add an (un)install butto to the titlebar
(defmethod title-bar-action-buttons ((system ql-dist:system))
  (if (ql-dist:installedp system)
      (action-button "Uninstall"
                     (thunk (ql-dist:uninstall system)
                            t))
      (action-button "Install"
                     (thunk (ql-dist:install system)
                            t))))

;; Dependency system as they are resolved by Quicklisp
(defview 👀dependencies (system ql-dist:system)
  (when-let (deps (ql-dist:required-systems system))
    (-<> deps
         (mapcar #'ql-dist:find-system <>)
         thunk
         (list-view :title "Dependencies" :priority 1))))

;; Other systems with the same name
(defview 👀homonyms (system ql-dist:system)
  (let ((systems (ql-dist::find-systems-named (ql-dist:name system))))
    (if (> (length systems) 1)
        (multi-column-list-view (thunk systems)
         :title "Homonyms"
         :priority 5
         :columns '("Preference" "Distribution" "Release")
         :display (list #'ql-dist:preference
                        (alexandria:compose #'ql-dist:name #'ql-dist:dist)
                        #'ql-dist:release))
        nil)))
