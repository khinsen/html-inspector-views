;;;; Thunks
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views)

(defclass thunk ()
  ((fn :reader fn :initarg :fn)))

(defmacro thunk (&body body)
  `(make-instance 'thunk :fn #'(lambda () ,@body)))

(defgeneric eval-thunk (object)
  (:method ((object t)) object)
  (:method ((thunk thunk)) (funcall (fn  thunk))))

