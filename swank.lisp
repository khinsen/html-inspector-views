;;;; Interface to swank or slynk
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

;;
;; Function name
;;

(defun function-name (fn)
  (swank-backend:function-name fn))

;;
;; Source code location for functions (including generic) and packages
;;

(defun source-code-location (object)
  (if (macro? object)
      (macro-source-code-location object)
      (non-macro-source-code-location object)))

(defun non-macro-source-code-location (object)
  (let ((location
          (-> object
            swank-backend:find-source-location)))
    (cond ((equal (car location) :location)
           (values (->> location cdr (assoc :file) cadr uiop/pathname:ensure-pathname)
                   (->> location cdr (assoc :position) cadr)))
          (t (values nil nil)))))

;; This function may be specific to SBCL, in which the name of a macro
;; function is (:macro name).
(defun macro? (object)
  (and (typep object 'function)
       (let ((fn-name (function-name object)))
         (and (consp fn-name)
              (eql :macro (car fn-name))))))

;; This function may be specific to SBCL, in which the name of a macro
;; function is (:macro name).
(defun macro-source-code-location (object)
  (let ((location
          (-<> object
            function-name
            cadr
            swank-backend:find-definitions
            (remove-if-not #'(lambda (def) (eq (caar def) 'defmacro)) <>)
            first
            second)))
    (cond ((equal (car location) :location)
           (values (->> location cdr (assoc :file) cadr uiop/pathname:ensure-pathname)
                   (->> location cdr (assoc :position) cadr)))
          (t (values nil nil)))))

;;
;; Definition location for vars
;;

(defun var-definition-location (symbol)
  (let ((location
          (-<> symbol
            swank-backend:find-definitions
            (remove-if-not #'(lambda (def) (eq (caar def) 'defvar)) <>)
            first
            second)))
    (cond ((equal (car location) :location)
           (values (->> location cdr (assoc :file) cadr uiop/pathname:ensure-pathname)
                   (->> location cdr (assoc :position) cadr)))
          (t (values nil nil)))))

;;
;; Callers
;;

(defun find-callers (fn)
  (->> fn
       function-name
       swank-backend:who-calls
       (mapcar #'car)
       (mapcar #'find-callable)))

(defun find-callable (dspec)
  (cond
    ((symbolp dspec)
     (symbol-function dspec))
    ((and (consp dspec)
          (equal 'cl:defmethod (car dspec)))
     (let* ((generic (fdefinition (cadr dspec)))
            (methods (c2mop:generic-function-methods generic))
            (specializers (car (last dspec)))
            (qualifiers (when (= 4  (length dspec)) (list (caddr dspec)))))
       (find-if #'(lambda (m)
                    (and (equal (mapcar #'swank-designator 
                                        (c2mop:method-specializers m))
                                specializers)
                         (equal (method-qualifiers m) qualifiers)))
                methods)))))

(defgeneric swank-designator (specializer)
  (:method ((specializer class))
    (class-name specializer))
  (:method ((specializer c2mop:eql-specializer))
    (list 'cl:eql (c2mop:eql-specializer-object specializer))))

;;
;; Specializers of classes
;;

(defun find-specializers (class)
  (->> class
       class-name
       swank-backend:who-specializes
       (mapcar #'(lambda (d) (find-specializer (car d) class)))
       (remove nil)))

(defun find-specializer (dspec class)
  (when (and (consp dspec)
             (equal 'cl:defmethod (car dspec)))
    (let* ((generic (fdefinition (cadr dspec)))
           (methods (c2mop:generic-function-methods generic)))
      (find-if #'(lambda (m)
                   (member (class-name class)
                           (mapcar #'swank-designator (c2mop:method-specializers m))
                           :test #'equal))
               methods))))

(defun find-applicable-methods (object)
  ;; TODO eql specializers
  (find-specializers-with-superclasses (class-of object)))

(defun find-specializers-with-superclasses (class)
  (let* ((direct (find-specializers class))
         (generic-fns (mapcar #'c2mop:method-generic-function direct))
         (superclasses (remove-if #'ignore-class?
                                  (c2mop:class-direct-superclasses class))))
    (flet ((generic-seen? (method)
             (member
              (c2mop:method-generic-function method)
              generic-fns)))
      (apply #'append
             (cons direct
                   (mapcar #'(lambda (sc)
                               (remove-if #'generic-seen?
                                          (find-specializers-with-superclasses sc)))
                           superclasses))))))

(defun ignore-class? (class)
  (let ((name (class-name class)))
    (cond
      ((eq name 't) t)
      ((eq name 'standard-object) t)
      ((not (external? name)) t)
      (t nil))))

(defun external? (symbol)
  (multiple-value-bind (_ status)
      (find-symbol (symbol-name symbol) (symbol-package symbol))
    (declare (ignore _))
    (eql status :external)))
