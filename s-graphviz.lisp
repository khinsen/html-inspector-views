;;;; Interface to s-graphviz
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

(defmethod dot-string ((object cons))
  (s-graphviz:format-graph object))
