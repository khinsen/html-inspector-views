;;;; Package definition
;;
;;;; Copyright (c) 2024-2025 Konrad Hinsen <konrad.hinsen@fastmail.net>

(defpackage :html-inspector-views/standard
  (:use :cl :html-inspector-views)
  (:import-from :alexandria
   :if-let :when-let :compose)
  (:import-from :arrow-macros
   :-> :-<> :->> :-<>> :<> :some-> :some->>)
  (:import-from :cl-who :esc :fmt :htm :str)
  (:export #:👀representations
           #:👀slots #:slot-names
           #:👀error-message
           #:👀symbols
           #:👀use-list
           #:👀content
           #:file-content-view
           #:mime-type
           #:*image*
           #:source-view #:source-code-view #:source-as-html
           #:var-definition
           #:parse-lisp-code
           #:render-as-html
           #:render-one-form-as-html
           #:render-toplevel-cst
           #:find-linked-object
           #:*current-source-code-file*
           #:json-wrap #:json-unwrap #:json-data
           #+quicklisp #:*quicklisp*))
