;;;; Views for Lisp source code
;;
;;;; Copyright (c) 2024-2025 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

;;
;; Eclector interface
;;
;; The concrete-syntax-tree variant of Eclector does most of what we need.
;;
;; We extend the client with a slot that stores the current package,
;; which is updated with every "in-package" form found at the top level.
;; This is a decent heuristic for assigning packages to symbols, but
;; it is not perfect. Lisp code can change *package* in many ways that are
;; not detectable by static code analysis.
;;

(defclass code-view-client (eclector.concrete-syntax-tree::cst-client)
  ((current-package :initform (find-package "CL-USER") :accessor current-package)))

(defmethod eclector.reader:state-value ((client code-view-client) (aspect (eql '*package*)))
  (current-package client))

(defmethod eclector.parse-result:make-expression-result
    ((client code-view-client) expression children source)
  (when (and (consp expression)
             (consp (cdr expression))
             (null (cddr expression))
             (eq (car expression) 'cl:in-package))
    (let ((package-name (cadr expression)))
      (setf (current-package client) (find-package package-name))))
  (call-next-method))

;; The standard client interns symbols as it encounters them. This is undesirable
;; in general because a code view shouldn't modify packages. But it also fails
;; for packages that are locked. We thus replace the symbol generation code
;; in such a way that non-existing symbols are replaced by a symbol-proxy
;; object that stores symbol name and package name.

(defclass symbol-proxy ()
  ((name :initarg :name)
   (package-name :initarg :package-name)))

(defmethod eclector.reader:interpret-symbol
    ((client code-view-client) input-stream (package-indicator t) symbol-name internp)
  (let* ((package (case package-indicator
                    (:current (current-package client))
                    (:keyword (find-package "KEYWORD"))
                    (t        (let ((*package* (current-package client)))
                                (find-package package-indicator))))))
    (multiple-value-bind (symbol status)
        (find-symbol symbol-name package)
      (if status
          symbol
          (make-instance 'symbol-proxy
                         :name symbol-name
                         :package-name (package-name package))))))

;; A nil value for package-indicator stands for uninterned symbols.

(defmethod eclector.reader:interpret-symbol
    ((client code-view-client) input-stream (package-indicator null) symbol-name internp)
  (declare (ignore client input-stream internp))
  (make-symbol symbol-name))

;; A view on concrete syntax trees for use in development.

(defview 👀items (cst cst:cons-cst)
  (-> (loop for item = cst then (cst:rest item)
            while (cst:consp item)
            collect (cst:first item))
    (multi-column-list-view :title "Items" :priority 1
                            :columns '("Source" "Raw")
                            :display (list #'cst:source #'cst:raw))))

;;
;; Class holding the source code and the concrete syntax tree
;;

(defclass lisp-code ()
  ((source :reader source :initarg :source)
   (concrete-syntax-tree :reader concrete-syntax-tree :initform nil)))

(defgeneric parse-lisp-code (input))

(defmethod parse-lisp-code ((input pathname))
  (parse-lisp-code (alexandria:read-file-into-string input)))

(defmethod parse-lisp-code ((input string))
  (let* ((client (make-instance 'code-view-client))
         (code (make-instance 'lisp-code :source input))
         (eclector.reader:*client* client)
         (eof-value (make-symbol "EOF")))
    (with-input-from-string (input (source code))
      (setf (slot-value code 'concrete-syntax-tree)
            (loop for toplevel-form = (read-with-recovery client input eof-value)
                  until (eq toplevel-form eof-value)
                  collect toplevel-form))
      code)))

(defun read-with-recovery (client input eof-value)
  (handler-bind
      ((reader-error #'(lambda (c)
                         (declare (ignore c))
                         (invoke-restart 'eclector.reader:recover))))
    (eclector.parse-result:read client input nil eof-value)))

(defview 👀source (code lisp-code)
  (-> code
    source
    (lisp-code-view :title "Source" :priority 1)))

(defview 👀read (code lisp-code)
  (-> code
    concrete-syntax-tree
    👀items
    (rename :title "CST" :priority 3)))

(defview 👀render (code lisp-code)
  (html-view :title "Rendered" :priority 5
    (render-as-html code)))

;;
;; A view showing a source code file with the toplevel form
;; at "offset" highlighted.
;;

(defvar *current-source-code-file* nil)

(defun source-view (pathname &optional offset)
  (when pathname
    (html-view :title "Source code" :priority 1
      (add-asset-path "/html-inspector-views/"
                      (asdf:system-relative-pathname
                       :html-inspector-views
                       "assets/html-inspector-views"))
      (include-js "/html-inspector-views/js/scroll-into-view.js")
      (include-script "scrollHighlightIntoView(window.currentInspectorView)")
      (when-let (c (find-component pathname))
        (html
          (:div :class "inspector-index" :style "text-align:right;padding-right:10px"
                (:small 
                 (:i (esc "system ")) (object-ref (asdf:component-system c))
                 (:i (esc ", component ")) (object-ref c)))))
      (let ((*current-source-code-file* pathname))
        (html
          (:div
           (-> pathname
             parse-lisp-code
             (render-as-html :highlight-at-offset offset))))))))

(defun single-form-source-view (pathname offset)
  (when pathname
    (html-view :title "Source code" :priority 1
      (let ((*current-source-code-file* pathname))
        (html
          (:div
           (let ((*current-source-code-file* pathname))
             (-> pathname
               parse-lisp-code
               (render-one-form-as-html offset)))))))))

(defun source-code-view (object &key in-file?)
  (multiple-value-bind (pathname offset)
      (source-code-location object)
    (if in-file?
        (source-view pathname offset)
        (single-form-source-view pathname offset))))

(defun source-as-html (object)
  (multiple-value-bind (pathname offset)
      (source-code-location object)
    (when pathname
      (-> pathname
          parse-lisp-code
          (render-one-form-as-html offset)))))

;;
;; Render code as HTML with syntax highlighting and embedded links
;;

;; Render the whole source code file, with one toplevel form at OFFSET highlighted

(defun render-as-html (code &key highlight-at-offset debug)
  (with-slots (source concrete-syntax-tree) code
    (when debug
      (html
        (object-ref concrete-syntax-tree
                    :display #'(lambda (_) (declare (ignore _)) "Concrete syntax tree"))
        (:hr)))
    (html
      (:pre
       (:code
        (let ((position 0))
          (dolist (toplevel-cst concrete-syntax-tree)
            (destructuring-bind (start . end) (cst:source toplevel-cst)
              (esc (str:substring position start source))
              (let ((highlight? (and highlight-at-offset
                                     (>= highlight-at-offset start)
                                     (< highlight-at-offset end)
                                     t)))
                (html
                  (:lisp-toplevel :highlight highlight?
                                  (setf position
                                        (render-toplevel-cst nil toplevel-cst
                                                             source start)))))
              (assert (eql position end))))
          (esc (str:substring position nil source))))))))

;; Render a single toplevel form at OFFSET

(defun render-one-form-as-html (code offset)
  (with-slots (source concrete-syntax-tree) code
    (html
      (:pre
       (:code
        (dolist (toplevel-cst concrete-syntax-tree)
          (destructuring-bind (start . end) (cst:source toplevel-cst)
            (when (and (>= offset start)
                       (< offset end))
              (html
                (:lisp-toplevel (render-toplevel-cst nil toplevel-cst
                                                      source start)))))))))))

;; For toplevel forms, dispatch on the head symbol

(defgeneric render-toplevel-cst (head cst source position))

(defmethod render-toplevel-cst ((head t) cst source position)
  (let ((linked-object (find-linked-object head cst)))
    (if linked-object
        (destructuring-bind (start . end) (cst:source cst)
          (let ((position (render-cst (cst:first cst) source start)))
            (html
              (esc (str:substring position start source))
              (:span :id (inspect-id linked-object)
                     :class "inspector-inspect"
                     (setf position (render-cst (cst:second cst) source position)))
              (esc (str:substring position end source)))
            end))
        (render-cst cst source position))))

(defmethod render-toplevel-cst ((head null) cst source position)
  (if (cst:consp cst)
      (when-let (head (-> cst cst:first cst:raw))
        (render-toplevel-cst head cst source position))
      (render-cst cst source position)))

(defgeneric find-linked-object (head cst))

;; Default: no linked object
(defmethod find-linked-object ((head t) cst)
  nil)

;; (in-package ...): link the package name to the package.
(defmethod find-linked-object ((head (eql 'in-package)) cst)
  (let ((package-name (-> cst cst:second cst:raw)))
    (find-package package-name)))

;; render-cst takes the source position up to which the code has
;; already been rendered, and returns the position up to which it
;; has progressed. Before rendering any part of the cst, the
;; text up to its start is rendered without annotations. That takes
;; care of whitespace and comments, but also source elements without
;; a source reference, such as quotes.

(defgeneric render-cst (cst source position))

(defmethod render-cst ((cst cst:cst) source position)
  (error "Unhandled CST subclass: ~a" cst))

(defmethod render-cst ((cst cst:cons-cst) source position)
  (if-let (cst-source (cst:source cst))
    (destructuring-bind (start . end) cst-source
      (html (esc (str:substring position start source)))
      (let ((position start))
        (loop for item = cst then (cst:rest item)
              while (cst:consp item)
              do (setf position
                       (render-cst (cst:first item) source position)))
        (html (esc (str:substring position end source))))
      end)
    position))

(defmethod render-cst ((cst cst:atom-cst) source position)
  (if (cst:source cst)
    (destructuring-bind (start . end) (cst:source cst)
      (when position
        (html (esc (str:substring position start source))))
      (let ((raw (cst:raw cst))
            (text (str:substring start end source)))
        (render-atom raw text))
      end)
    ;; An atom-cst without source reference occurs e.g. for a a
    ;; quoting apostrophe. We ignore it and return the current
    ;; position, to ensure that the next render-cst call outputs the
    ;; missing text.
    position))

(defgeneric render-atom (atom text))

(defmethod render-atom ((atom t) text)
  (html (esc text)))

(defmethod render-atom ((atom symbol) text)
  (let ((package (symbol-package atom)))
    (cond
      ((null package)
       (html
         (:lisp-uninterned-symbol :name (symbol-name atom) (esc text))))
      ((eq package (find-package "KEYWORD"))
       (html
         (:lisp-keyword :name (symbol-name atom) (esc text))))
      ((eq package (find-package "COMMON-LISP"))
       (html
         (:lisp-symbol :package "CL"
                       :name (symbol-name atom)
                       :id (inspect-id (make-hyperspec-page atom))
                       (esc text))))
      (t
       (if-let (refs (symbol-refs atom))
         (html
           (:lisp-symbol :package (package-name package)
                         :name (symbol-name atom)
                         :id (eval-id refs)
                         (esc text)))
         (html
           (:lisp-symbol :package (package-name package)
                         :name (symbol-name atom)
                         (esc text))))))))

(defmethod render-atom ((atom symbol-proxy) text)
  (with-slots (name package-name) atom
    (html
      (:lisp-symbol :package package-name
                    :name name
                    (esc text)))))

(defmethod render-atom ((atom number) text)
  (html
    (:lisp-number (esc text))))

(defmethod render-atom ((atom string) text)
  (html
    (:lisp-string (esc text))))

(defmethod render-atom ((atom array) text)
  (html
    (:lisp-array (esc text))))

(defmethod render-atom ((atom character) text)
  (html
    (:lisp-character (esc text))))

(defmethod render-atom ((atom pathname) text)
  (html
    (:lisp-pathname (esc text))))

;;
;; Get the object that a symbol refers to. If there are several such
;; objects, return a reference object that groups them together.
;;

(defclass symbol-refs ()
  ((symbol :initarg :symbol)
   (refs :initarg :refs)))

(defclass var-definition ()
  ((symbol :initarg :symbol)
   (pathname :initarg :pathname)
   (offset :initarg :offset)))

(defun var-definition (symbol)
  (let ((var-loc (multiple-value-list (var-definition-location symbol))))
    (when (first var-loc)
      (make-instance 'var-definition
                     :symbol symbol
                     :pathname (first var-loc)
                     :offset (second var-loc)))))

(defun symbol-refs (symbol)
  (let ((refs '())
        (var-def-loc (var-definition symbol)))
    (when var-def-loc
      (push (cons :var var-def-loc) refs))
    (when (fboundp symbol)
      (push (cons :function (symbol-function symbol)) refs))
    (when-let (class (find-class symbol nil))
      (push (cons :class class) refs))
    (when-let (plist (symbol-plist symbol))
      (push (cons :plist plist) refs))
    (cond
      ((null refs) nil)
      ((eql 1 (length refs)) (cdar refs) )
      (t (make-instance 'symbol-refs :symbol symbol :refs (reverse refs))))))

(defmethod text-representation ((refs symbol-refs))
  (-> refs
    (slot-value 'symbol)
    symbol-name
    str:downcase))

(defview 👀references (refs symbol-refs)
  (with-slots (refs) refs
    (multi-column-list-view refs
                            :title "References" :priority 1
                            :columns '("Type" "Object")
                            :display (list #'(lambda (r) (symbol-name (car r))) #'cdr)
                            :inspect-items :by-column)))

(defview 👀var (refs symbol-refs)
  (with-slots (refs) refs
    (when-let (pair (assoc :var refs))
      (-> pair
        cdr
        👀source
        (rename :title "Var" :priority 4)))))

(defview 👀function (refs symbol-refs)
  (with-slots (refs) refs
    (when-let (pair (assoc :function refs))
      (-> pair
        cdr
        👀source
        (rename :title "Function" :priority 5)))))

(defview 👀class (refs symbol-refs)
  (with-slots (refs) refs
    (when-let (pair (assoc :class refs))
      (-> pair
        cdr
        👀source
        (rename :title "Class" :priority 6)))))

(defview 👀plist (refs symbol-refs)
  (with-slots (refs) refs
    (when-let (pair (assoc :plist refs))
      (-<> pair
        cdr
        (loop for (p v) on <> by #'cddr collect (cons p v))
        (multi-column-list-view :title "Plist" :priority 7
                                :columns '("Property" "Value")
                                :display (list #'car #'cdr))))))

(defmethod text-representation ((var-def var-definition))
  (-> var-def
    (slot-value 'symbol)
    symbol-name
    str:downcase))

(defview 👀source (var-def var-definition)
  (with-slots (pathname offset) var-def
      (source-view pathname offset)))
