;;;; Package definition
;;
;;;; Copyright (c) 2024-2025 Konrad Hinsen <konrad.hinsen@fastmail.net>

(defpackage :html-inspector-views
  (:use :cl)
  (:import-from :cl-who :esc :fmt :htm :str)
  (:export :esc :fmt :htm :str
           #:view
           #:html-view #:make-html-view
           #:html #:html-and-references
           #:view-object #:view-method #:view-title #:view-priority
           #:view-html #:view-references #:view-assets #:view-create-html
           #:all-views
           #:rename #:priority
           #:defview
           #:👀items
           #:👀description
           #:👀representations
           #:👀content
           #:👀source
           #:👀print-string
           #:open-package-operations?
           #:text-representation
           #:html-representation
           #:title-bar #:title-bar-representation #:title-bar-action-buttons
           #:gui-class
           #:pane-title
           #:thunk #:eval-thunk
           #:inspect-id #:action-id #:eval-id #:input-id
           #:object-ref #:action-button #:eval-button
           #:get-input #:*get-input-fn*
           #:add-asset-path #:include-js #:include-css #:include-script
           #:transclusion
           #:html-table
           #:list-view
           #:enumerated-list-view
           #:multi-column-list-view
           #:key-value-table-view
           #:tree-view
           #:lisp-code-view #:html-code-view
           #:include-lisp-highlight-assets #:include-html-beautify-assets
           #:lisp-snippet #:html-snippet
           #:graphviz-view #:dot-string
           #:encode-base32 #:decode-base32
           #:select #:checkboxes
           #:parametrized-subview #:view-thunk #:view-vars
           #:get-view-var #:set-view-var
           #:*get-view-var-fn* #:*set-view-var-fn*))
