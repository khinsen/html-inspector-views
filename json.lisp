;;;; Views for JSON data
;;
;;;; Copyright (c) 2025 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

;;
;; JSON parsers return Lisp data structures such as lists and hash tables.
;; They need to be wrapped into a class to display views specific to their
;; interpretation as parsed JSON data
;;

(defclass json-data ()
  ((data :accessor data :initarg :data)))

(defun json-wrap (item)
  (make-instance 'json-data :data item))

(defun json-unwrap (item)
  (assert (typep item 'json-data))
  (slot-value item 'data))

(defgeneric json-text-representation (data)
  (:method ((data t))
    (text-representation data)))

(defmethod json-text-representation ((data hash-table))
  (with-output-to-string (s)
    (write-string "{" s)
    (loop for key being the hash-keys in data
            using (hash-value value)
          do (format s "~a: ~a, " key (json-text-representation value)))
    (write-string "}" s)))

(defmethod json-text-representation ((data cons))
  (with-output-to-string (s)
    (write-string "[" s)
    (loop for value in data
          do (format s "~a, " (json-text-representation value)))
    (write-string "]" s)))

(defmethod json-text-representation ((data vector))
  (with-output-to-string (s)
    (write-string "[" s)
    (loop for value across data
          do (format s "~a, " (json-text-representation value)))
    (write-string "]" s)))

(defmethod json-text-representation ((data string))
  (format nil "\"~a\"" data))

(defmethod text-representation ((data json-data))
  (json-text-representation (json-unwrap data)))

(defgeneric json-view (data)
  (:method ((data t))
    (html-view :title "JSON" :priority 1
     (html
       (esc (text-representation data))))))

(defmethod json-view ((data cons))
  (-<> data
    (mapcar #'json-wrap <>)
    👀items
    (rename :title "JSON" :priority 1)))

(defmethod json-view ((data vector))
  (if (typep data 'string)
      (call-next-method)
      (-<> data
        (coerce 'list)
        json-view)))

(defmethod json-view ((data hash-table))
  (-> data
    (alexandria:copy-hash-table :key #'json-wrap)
    👀items
    (rename :title "JSON" :priority 1)))

(defview 👀json (data json-data)
  (json-view (json-unwrap data)))
