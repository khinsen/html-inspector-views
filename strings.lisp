;;;; Views for string objects
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

;;
;; Action button "copy to clipboard"
;;

(defmethod title-bar-action-buttons ((string string))
  (action-button *icon-copy*
                 (thunk (trivial-clipboard:text string)
                        nil)))
