;;;; Views for pathname objects
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

;;
;; Components view
;;

(defview 👀components (object pathname)
  (-> object
      pathname-components
      thunk
      (key-value-table-view :title "Components"
                            :priority 5)))

(defun pathname-components (p)
  (list (cons "Host" (pathname-host p))
        (cons "Device" (pathname-device p))
        (cons "Directory" (pathname-directory p))
        (cons "Name" (pathname-name p))
        (cons "Type" (pathname-type p))
        (cons "Version" (pathname-version p))))

;;
;; Path view
;;

(defview 👀path (object pathname)
  (when (equal :absolute
               (first (pathname-directory object)))
    (-> object
        list
        path-parents
        thunk
        (multi-column-list-view
         :title "Path"
         :priority 4
         :columns '("" "Name")
         :display (list #'(lambda (p)
                            (if (probe-file p) "✓" "❌"))
                        #'(lambda (p)
                            (if-let (name (pathname-name p))
                              (princ-to-string
                               (make-pathname :name name :type (pathname-type p)))
                              (let ((dir (pathname-directory p)))
                                (cond
                                  ((equal dir '(:absolute)) "/")
                                  (t (car (last dir))))))))))))

(defun path-parents (pathnames)
  (let ((top (first pathnames)))
    (if (uiop:directory-pathname-p top)
        (let ((parent (uiop:pathname-parent-directory-pathname top)))
          (if (equal parent top)
              pathnames
              (path-parents (cons parent pathnames))))
        (let ((directory (make-pathname :directory (pathname-directory top))))
          (path-parents (cons directory pathnames))))))

;;
;; Directory view
;;

(defview 👀items (object pathname)
  (unless (or (pathname-name object)
              (zerop (+ (length (uiop:subdirectories object))
                        (length (uiop:directory-files object)))))
    (let ((directories (uiop:subdirectories object))
          (files (uiop:directory-files object)))
      (html-view :title "Items" :priority 1
        (html
          (:details :open t
                    (:summary (fmt "~a directories" (length directories)))
                    (:table :class "inspector-table" 
                            (directory-subdirectories directories)))
          (:details :open t
                    (:summary (fmt "~a files" (length files)))
                    (:table :class "inspector-table"
                            (directory-files files))))))))

(defun directory-subdirectories (dirs)
  (loop
    for dir in dirs
    and i from 0
    do
    (html
      (:tr (:td
            (object-ref dir
                        :display #'(lambda (dir)
                                     (car (last (pathname-directory dir))))))))))

(defun directory-files (files)
  (loop
    for file in files
    and i from 0
    do
    (html
     (:tr (:td
           (object-ref file
                       :display #'(lambda (file)
                                    (-> (make-pathname :name (pathname-name file)
                                                       :type (pathname-type file))
                                        princ-to-string))))))))

;;
;; Content view
;;

(defview 👀content (object pathname)
  (when (and (pathname-name object)        ; test for file
             (uiop:file-exists-p object))  ; test for existence
    (file-content-view (-> object
                      pathname-type
                      str:upcase
                      alexandria:make-keyword)
                  object)))

;; Dispatch by pathname-type

(defgeneric file-content-view (type pathname))

(defmethod file-content-view ((type t) pathname)
  nil)

(defmethod file-content-view ((type (eql :nil)) pathname)
  nil)

(defmethod file-content-view ((type (eql :jpg)) pathname)
  (image-view pathname))

(defmethod file-content-view ((type (eql :jpeg)) pathname)
  (image-view pathname))

(defmethod file-content-view ((type (eql :png)) pathname)
  (image-view pathname))

(defmethod file-content-view ((type (eql :gif)) pathname)
  (image-view pathname))

;; Images are rendered directly by the browser

(defgeneric mime-type (type))

(defmethod mime-type ((type (eql :jpg)))
  "image/jpeg")

(defmethod mime-type ((type (eql :jpeg)))
  "image/jpeg")

(defmethod mime-type ((type (eql :png)))
  "image/png")

(defmethod mime-type ((type (eql :gif)))
  "image/gif")

(defun image-view (pathname)
  (make-html-view
   (thunk (let* ((bytes (alexandria:read-file-into-byte-vector pathname))
                 (encoded (base64:usb8-array-to-base64-string bytes))
                 (mime (mime-type (-> pathname pathname-type str:upcase alexandria:make-keyword)))
                 (data-url (str:concat "data:" mime ";base64," encoded)))
            (values (format nil "<img src=\"~a\" width=\"100%\"/>" data-url)
                    nil)))
   :title "Image"
   :priority 1))

;;
;; Action button "copy to clipboard"
;;

(defmethod title-bar-action-buttons ((pathname pathname))
  (action-button *icon-copy*
                 (thunk (trivial-clipboard:text (princ-to-string pathname))
                        nil)))
