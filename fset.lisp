;;;; Views for FSet objects
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

(defmethod text-representation ((object fset:bag))
  (format nil "fset:bag :size ~a"
          (fset:size object)))

(defview 👀items (object fset:bag)
  (unless (fset:empty? object)
    (list-view (thunk (fset:convert 'list object)) :title "Items" :priority 5)))

(defmethod text-representation ((object fset:map))
  (format nil "fset:map :size ~a :default ~a"
          (fset:size object)
          (fset:map-default object)))

(defview 👀items (object fset:map)
  (unless (fset:empty? object)
    (html-view :title "Items" :priority 5
      (html
        (:small :class "inspector-index"
                (fmt "~a items" (fset:size object)))
        (:table :class "inspector-table"
                (:tr (:th "Key") (:th "Value"))
                (fset:do-map (key value object)
                  (html
                    (:tr :class "inspector-inspect"
                         (:td (object-ref key))
                         (:td (object-ref value))))))))))

(defmethod text-representation ((object fset:set))
  (format nil "fset:set :size ~a"
          (fset:size object)))

(defview 👀items (object fset:seq)
  (unless (fset:empty? object)
    (enumerated-list-view
     (thunk (fset:convert 'vector object))
     :title "Items" :priority 5)))

(defmethod text-representation ((object fset:seq))
  (format nil "fset:seq :size ~a"
          (fset:size object)))

(defview 👀items (object fset:set)
  (unless (fset:empty? object)
    (list-view (thunk (fset:convert 'list object)) :title "Items" :priority 5)))
