;;;; Views for conditions
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

;;
;; Render conditions in CSS class "inspector-error"
;;

(defmethod html-representation ((object condition) &optional id)
  (let ((text (text-representation object)))
    (if id
        (html (:span :id id :class "inspector-error" (esc text)))
        (html (:span :class "inspector-error" (esc text))))))

;;
;; Error message view
;;

(defview 👀error-message (object condition)
  (html-view :title "Error" :priority 1
    (html (str (html-representation object)))))
