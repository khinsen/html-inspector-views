;;;; Templates for use in inspector views
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/templates)

;;
;; Make an anonymous no-argument template, and insert the
;; corresponding code into the current HTML output.
;;

(defmacro template (html-string)
  (let ((code
         (-<> html-string
              ;; wrap into a block, as required by the eco parser
              (format nil "<% template %>~%~A<% end %>~%" <>)
              ;; parse
              eco.parser:parse-template
              ;; compile in the current package
              (eco.compiler:compile-template *package*)
              ;; strip off "progn" and take first element
              cadr
              ;; strip off "template"
           cdr)))
    `(let ((eco-template::%eco-stream html-inspector-views::*html-stream*))
       ,@code)))

