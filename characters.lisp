;;;; Views for characters
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

(defmethod text-representation ((char character))
  (char-name char))
