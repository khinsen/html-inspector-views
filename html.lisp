;;;; Generation of HTML with object references
;;
;;;; Copyright (c) 2024-2025 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views)

;;
;; HTML streams with an associated map from HTML ids to objects
;;

(defclass view-accumulator ()
  ((references :accessor accumulator-references :initform nil)
   (assets :accessor accumulator-assets :initform nil)))

(defvar *html-stream*)
(defvar *view-accumulator*)

(defmacro html (&body body)
  `(cl-who:with-html-output (*html-stream*)
    ,@body))

(defmacro html-and-references (&body body)
  (let ((html-mode-sym (gensym))
        (html-sym (gensym)))
    `(let ((*view-accumulator* (make-instance 'view-accumulator))
           (,html-mode-sym (cl-who:html-mode)))
       (setf (cl-who:html-mode) :html5)
       (let ((,html-sym (with-output-to-string (*html-stream*)
                          (html ,@body))))
         (setf (cl-who:html-mode) ,html-mode-sym)
         (values ,html-sym
                 (accumulator-references *view-accumulator*)
                 (accumulator-assets *view-accumulator*))))))

;;
;; The text representation of an object defaults to its print representation
;;

(defgeneric text-representation (object)
  (:documentation "Return a text representation of object for use in views and
title bars. Defaults to write-to-string with :escape nil :readably nil.")
  (:method ((object t))
    (write-to-string object :length 100 :escape nil :readably nil)))

;;
;; The HTML representation of an object defaults to its text representation
;;

(defgeneric html-representation (object &optional id)
  (:documentation "Generate an HTML string representing the object. The default
is a <span> containing the object's text-representation. If id is given, it
becomes the HTML element's id.")
  (:method ((object t) &optional id)
    (let ((text (text-representation object)))
      (if id 
          (html (:span :id id (esc text)))
          (html (:span (esc text)))))))

;;
;; Generate object references and embedding HTML
;;

(defun html-id (prefix object &key (select nil))
  (let* ((base-html-id (symbol-name (gensym prefix)))
         (html-id (if select
                      (concatenate 'string base-html-id "-" (encode-base32 select))
                      base-html-id)))
    (push (cons html-id object) (accumulator-references *view-accumulator*))
    html-id))

(defun inspect-id (object &key (select nil))
  (html-id "inspect-" object :select select))

(defun action-id (thunk)
  (html-id "action-" thunk))

(defun eval-id (thunk)
  (html-id "eval-" thunk))

(defun object-ref (object &key (display nil) (highlight nil) (select nil))
  (let ((html-id (inspect-id object :select select))
        (label (typecase display
                 (null object)
                 (function (funcall display object))
                 (t display))))
    (html
      (:span :class (if highlight
                        "inspector-inspect inspector-highlight"
                        "inspector-inspect")
             (html-representation label html-id)))))

(defun action-button (label thunk)
  (html
    (:button :id (action-id thunk)
             :class "inspector-action"
             (str label))))

(defun eval-button (label thunk)
  (html
    (:button :id (eval-id thunk)
             :class "inspector-action"
             (str label))))

;;
;; Add an asset unless it's already in place
;;

(defun add-asset (asset)
  (unless (member asset (accumulator-assets *view-accumulator*)
                  :test #'equal)
    (push asset (accumulator-assets *view-accumulator*))))

;;
;; Add JavaScript and CSS assets, to be loaded before the view is rendered
;;

(defun add-asset-path (url-prefix pathname)
  (add-asset (list :path url-prefix pathname)))

(defun include-js (url)
  (add-asset (cons :js url)))

(defun include-css (url)
  (add-asset (cons :css url)))

;;
;; Add a script asset, which will be run each time a view is
;; displayed, after the DOM has been constructed in the browser.
;;
;; The script can access the view element as 'window.currentInspectorView'.
;;

(defun include-script (js-code)
  (add-asset (cons :script js-code)))

;;
;; Add a transclusion
;;

(defun transclusion (view)
  (let ((html-id (symbol-name (gensym "transclusion-"))))
    (html
      (:div :id html-id))
    (push (cons html-id view) (accumulator-references *view-accumulator*))))
