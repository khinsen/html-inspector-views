// Scroll the first element with attribute "highlight" into view.
//
// Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>
//
scrollHighlightIntoView = function(viewElement) {
    const el = document.querySelector('[highlight]');
    el.scrollIntoView();
};
