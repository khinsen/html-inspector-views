// Highlight Lisp code elements in a view
//
// Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>
//
highlightLispCodeElements = function(element) {
    element.querySelectorAll('pre code').forEach((el) => {
        if (el.classList.contains("language-lisp")) {
            hljs.highlightElement(el);
        }
  });
};
