// Process <graphviz-element> elements in a view
//
// Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>
//
function isInspectorHtmlId (s) {
    var parts = s.split('-');
    if (parts.length !== 2) {
        return false;
    }
    if (!['inspect', 'action', 'eval'].includes(parts[0])) {
        return false;
    }
    if (isNaN(Number(parts[1]))) {
        return false;
    }
    return true;
}

function processGraphvizElements(element) {
    Viz.instance().then(function(viz) {
        var graphvizElements = element.getElementsByTagName('graphviz-element');
        for(var i = 0; i < graphvizElements.length; ++ i)
        {
            var e = graphvizElements[i];
            var options = Array.from(e.attributes)
                .map(a => [a.name, a.value])
                .reduce((acc, attr) => {
                    acc[attr[0]] = attr[1]
                    return acc
                }, {});
            var svg = viz.renderSVGElement(e.textContent, options);
            var gs = svg.getElementsByTagName('g');
            for(var i = 0; i < gs.length; ++ i) {
                var g = gs[i];
                var t = g.getElementsByTagName('title');
                if (t.length == 1) {
                    var s = t[0].textContent;
                    if (isInspectorHtmlId(s)) {
                        g.id = s;
                    }
                }
            }
            e.textContent = '';
            e.appendChild(svg);
        }
    });
};

