// Beautify and highlight <html-code> elements in a view
//
// Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>
//
processHtmlCodeElements = function(element) {
    var htmlCodeElements = element.getElementsByTagName('html-code');
    hljs.configure({ignoreUnescapedHTML: true});
    for(var i = 0; i < htmlCodeElements.length; ++ i)
    {
        var e = htmlCodeElements[i];
        var p = document.createElement('pre');
        var c = document.createElement('code');
        c.textContent = html_beautify(e.textContent);
        c.classList.add("language-html");
        p.appendChild(c)
        e.textContent = '';
        e.appendChild(p);
        hljs.highlightElement(p);
    }
};
