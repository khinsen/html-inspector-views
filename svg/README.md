The SVG icons in this directory were obtained from
[SVG Repo](https://www.svgrepo.com/). Except for adjusting the
size, they are unmodified.

The download links are:

- icon-copy.svg:      https://www.svgrepo.com/svg/24801/copy
- icon-eval.svg:      https://www.svgrepo.com/svg/80611/scroll

All these icons are distributed under a CC0 license.
