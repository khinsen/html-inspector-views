;;;; Views on view objects
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

;;
;; Source code
;;

(defview 👀source (object view)
  (-> object
      view-method
      👀source
      (rename :title "Source code" :priority 2)))

;;
;; The view itself
;;

(defview 👀view (object view)
  ;; The funcall makes a copy of the view. This is necessary
  ;; because the slot view-method of the view is modified
  ;; by an :around method.
  (-> (funcall (c2mop:method-generic-function (view-method object))
               (view-object object))
      (rename :title "View" :priority 1)))

;;
;; HTML code
;;

(defview 👀assets (object view)
  (when-let (assets (view-assets object))
    (html-view :title "Assets" :priority 4
      (html
        (:small :class "inspector-index"
                (fmt "~a item(s)" (length assets)))
        (:table :class "inspector-table"
                (:tr (:th (esc "Type")) (:th (esc "URL")))
                (loop for (type . url ) in assets do
                  (html
                    (:tr
                     (:td (esc (-> type symbol-name str:downcase)))
                     (:td (:a :href url :target "_blank" (esc (text-representation url))))))))))))

;;
;; References and actions
;;

(defview 👀references (object view)
  (reference-view object "inspect" "References" 5))

(defview 👀actions (object view)
  (reference-view object "action" "Actions" 6))

(defun reference-view (object reftype title priority)
  (when-let (refs (->> object
                       view-references
                       (remove-if-not #'(lambda (pair)
                                          (string= reftype
                                                   (->> pair
                                                        car
                                                        (str:split "-")
                                                        first))))))
    (multi-column-list-view refs
                            :title title
                            :priority priority
                            :columns '("Id" "Object")
                            :display (list #'(lambda (pair) (car pair))
                                           #'(lambda (pair) (cdr pair))))))

