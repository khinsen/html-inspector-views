;;;; Manage SVG files
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

;;
;; Load SVG files from the svg directory in this repository
;;

(defvar *svg-directory*
  (asdf:system-relative-pathname :html-inspector-views "svg/"))

(defun load-svg-icon (filename)
  (-> filename
      (merge-pathnames *svg-directory*)
      alexandria:read-file-into-string))

(defvar *icon-copy* (load-svg-icon "icon-copy.svg"))
(defvar *icon-eval* (load-svg-icon "icon-eval.svg"))
