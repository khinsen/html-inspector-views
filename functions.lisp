;;;; View showing the source code of a function
;;
;;;; Copyright (c) 2024 Konrad Hinsen <konrad.hinsen@fastmail.net>

(in-package #:html-inspector-views/standard)

;;
;; Function name as text representation
;;

(defmethod text-representation ((object function))
  (if-let ((name (function-name object)))
    (let ((*package* (find-package 'common-lisp)))
      (-> name prin1-to-string str:downcase))
    (call-next-method)))

;;
;; Source code view
;;

(defview 👀source (object function)
  (source-code-view object :in-file? t))

;;
;; Methods of a generic function
;;

(defview 👀methods (fn generic-function)
  (list-view (c2mop:generic-function-methods fn)
             :title "Methods"
             :priority 3))

;;
;; Callers
;;

(defview 👀callers (fn function)
  (when-let (callers (find-callers fn))
    (-> callers
        (sort #'string< :key #'text-representation)
        (list-view :title "Callers"
                   :priority 4
                   :display #'text-representation))))

;;
;; Methods
;;

(defmethod text-representation ((method method))
  (let ((*package* (find-package 'common-lisp)))
    (method-text-representation method)))

(defun method-text-representation (method)
  (format nil "~a ~{~a ~} ~a"
          (method-fn-name method)
          (->> method
               method-qualifiers
               (mapcar #'(lambda (s)
                           (->> s symbol-name str:downcase (str:concat ":")))))
          (->> method
               c2mop:method-specializers
               (mapcar #'text-representation))))

(defun method-fn-name (method)
  (-> method
      c2mop:method-generic-function
      function-name
      prin1-to-string
      str:downcase))

(defmethod text-representation ((eqs c2mop:eql-specializer))
  (format nil "(eql ~a)"
          (text-representation (c2mop:eql-specializer-object eqs))))

;; The generic function and its specializers

(defview 👀generic (method method)
  (html-view :title "Generic" :priority 2
    (html
     (:h4 (esc "Generic function"))
     (:div (object-ref (c2mop:method-generic-function method)))
     (when-let (qs (method-qualifiers method))
       (html
         (:h4 (esc "Qualifiers"))
         (html-table qs :display (list #'(lambda (s)
                                           (->> s
                                                symbol-name
                                                str:downcase
                                                (str:concat ":")))))))
     (:h4 (esc "Specializers"))
     (html-table (c2mop:method-specializers method)))))

;; Source code

(defview 👀source (object method)
  (source-code-view object :in-file? t))

;;
;; Thunks
;;

(defview 👀source (object thunk)
  (👀source (html-inspector-views::fn object)))
